import 'reflect-metadata';

import { COMMAND_VALIDATOR_METADATA } from '../constants';
import { ICommand } from '../ICommand';

export const CommandValidator = (command: ICommand): ClassDecorator => {
  // eslint-disable-next-line  @typescript-eslint/ban-types
  return (target: object) => {
    Reflect.defineMetadata(COMMAND_VALIDATOR_METADATA, command, target);
  };
};
