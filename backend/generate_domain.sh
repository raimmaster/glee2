set -x
echo $1

nest -- generate module $1
nest -- generate controller $1

mkdir "src/$1/entities"
touch "src/$1/entities/$1.entity.ts"

mkdir "src/$1/repositories"
touch "src/$1/repositories/$1.repository.ts"

mkdir "src/$1/events"
mkdir "src/$1/events/handlers"
touch "src/$1/events/handlers/index.ts"

mkdir "src/$1/commands"
mkdir "src/$1/commands/handlers"
touch "src/$1/commands/handlers/index.ts"
mkdir "src/$1/commands/validators"
touch "src/$1/commands/validators/index.ts"

mv "src/$1" "src/modules/domain/$1"