using System;
using System.Threading.Tasks;
using Data;
using Microsoft.EntityFrameworkCore;

namespace Web.Crud.Specs
{
    public static class TestAppDataContext
    {
        public static AppDataContext GetInstance()
        {
            var options = new DbContextOptionsBuilder<AppDataContext>()
                .UseSqlite("Data Source=crud_testing.db")
                .Options;

            var appDataContext = new AppDataContext(options);
            appDataContext.Database.EnsureCreated();
            return appDataContext;
        }

        public static void Clean(this AppDataContext appDataContext)
        {
            appDataContext.Database.EnsureDeleted();
        }
    }
}