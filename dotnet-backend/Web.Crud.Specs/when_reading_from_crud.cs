using System;
using System.Collections.Generic;
using Data;
using Domain.Employees;
using FizzWare.NBuilder;
using Machine.Specifications;
using Microsoft.AspNetCore.Http;
using Web.TestingSupport;

namespace Web.Crud.Specs
{
    public class when_reading_from_crud
    {
        static AppDataContext _appDataContext;
        static EmployeesController _crudController;
        static Guid _id;
        const string FirstName = "testing1";
        static Employee _employee;
        
        Establish _context = () =>
        {
            _id = Guid.NewGuid();

            var firstDbContext = TestAppDataContext.GetInstance();
            firstDbContext.Set<Employee>().AddAsync(Builder<Employee>.CreateNew()
                .With(x => x.Id, _id)
                .Build());
            firstDbContext.SaveChangesAsync();
            
            _appDataContext = TestAppDataContext.GetInstance();
            _crudController = new EmployeesController(_appDataContext);
        };

        Because of = () => { _employee = _crudController.GetOne(_id).Await(); };

        It should_be_accessible_from_entity_framework = () => { _employee.Id.ShouldEqual(_id); };

        Cleanup after = () => _appDataContext.Clean();
    }
}