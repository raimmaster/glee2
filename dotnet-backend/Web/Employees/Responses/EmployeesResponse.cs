﻿using System.Collections.Generic;
using Data.Projections;

namespace Web.Employees.Responses
{
    public class EmployeesResponse
    {
        public EmployeesResponse()
        {
            Data = new List<EmployeeListing>();
        }
        public EmployeesResponse(List<EmployeeListing> employeeListings, int currentPage, int lastPage, int perPage, int total)
        {
            Data = employeeListings;
            CurrentPage = currentPage;
            LastPage = lastPage;
            PerPage = perPage;
            Total = total;
        }

        public List<EmployeeListing> Data { get; set; }
        public int CurrentPage { get; set; }
        public int LastPage { get; set; }
        public int PerPage { get; set; }
        public int Total { get; set; }
        
    }
}