namespace Web.Infrastructure
{
    public class ApiInformation
    {
        protected ApiInformation()
        {
            Version = "1.0.0.0";
        }

        public ApiInformation(string version)
        {
            Version = version;
        }

        public string Version { get; protected set; }
    }
}