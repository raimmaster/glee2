using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
using Web.Infrastructure.ExceptionHandling.Renderers;

namespace Web.Infrastructure.ExceptionHandling
{
    [Boilerplate]
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ExceptionHandlerMiddleware
    {
        readonly RequestDelegate next;
        readonly IEnumerable<IExceptionRender> _renders;

        public ExceptionHandlerMiddleware(RequestDelegate next, IEnumerable<IExceptionRender> renders)
        {
            this.next = next;
            _renders = renders;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            if(context == null)
                throw new ArgumentNullException(nameof(context));
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                var render = _renders.FirstOrDefault(exceptionRender => exceptionRender.ShouldHandle(ex));
                if (render == null)
                {
                    Log.Error(ex, "Exception while handling API request.", context.Request.Path);
                    await HandleExceptionAsync(context, ex);
                }
                else
                {
                    await render.Render(context, ex);
                }
            }
        }

        static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;

            var result = JsonConvert.SerializeObject(new {error = exception.Message});
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) code;
            return context.Response.WriteAsync(result);
        }
    }
}