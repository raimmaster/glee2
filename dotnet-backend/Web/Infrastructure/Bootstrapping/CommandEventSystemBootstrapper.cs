using System;
using Avenue.AspNetCore;
using Avenue.Commands;
using Avenue.Domain;
using Avenue.EventSourcing;
using Avenue.EventSourcing.EntityFramework;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using Avenue.Worker;
using Avenue.Worker.Commands;
using Avenue.Worker.Events;
using Data.Projections;
using Data.Projections.EventHandlers;
using Data.Projections.Validations;
using Domain.Employees;
using Domain.Employees.Commands.Handlers;
using Domain.Employees.Commands.Validators.ForCreateEmployee;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.Infrastructure.Bootstrapping.Dependencies;
using Worker.AspNetCore;

namespace Web.Infrastructure.Bootstrapping
{
    public static class CommandEventSystemBootstrapper
    {
        public static void ConfigureCommandEventSystem(this IServiceCollection services,
            IConfiguration configuration)
        {
            var connectionString = configuration["DB_CONNECTION"];

            void ViewModelDatabaseOptions(DbContextOptionsBuilder options)
            {
                if (configuration.IsDevelopment() || string.IsNullOrEmpty(connectionString))
                    options.UseSqlite("Data Source=viewModel_testing.db");
                else
                    options.UseNpgsql(connectionString);

                options.EnableSensitiveDataLogging();
            }

            var domainEvents = new PubSub<IDomainEvent>();
            var commandQueue = new PubSub<ICommand>();

            services.ConfigureLocalCommandDispatcher(commandQueue, ViewModelDatabaseOptions);

            //EE1:START LEAVE THIS CODE IF YOU WANT EVENT SOURCING (but remove the comment)
            ConfigureEventSourcing(services, configuration, connectionString, domainEvents, commandQueue);
            services.ConfigureDomainEventWorker(domainEvents, ViewModelDatabaseOptions);
            //EE1:END


            //RP1:START LEAVE THIS CODE IF YOU WANT RELATIONAL (but remove the other code and the conditional)
            // ConfigureRelationalPersistence(services, configuration, connectionString, domainEvents, commandQueue);
            //RP1:END
        }

        static void ConfigureEventSourcing(IServiceCollection services, IConfiguration configuration, string connectionString,
            PubSub<IDomainEvent> domainEvents, PubSub<ICommand> commandQueue)
        {
            void EventSourcingDbContextConfig(DbContextConfig<EventSourcingDbContext> db) =>
                db.WithDbOptions(options =>
                {
                    if (configuration.IsDevelopment() || string.IsNullOrEmpty(connectionString))
                        options.UseSqlite("Data Source=command_testing.db");
                    else
                        options.UseNpgsql(connectionString ?? throw new ArgumentNullException(nameof(connectionString)));

                    options.EnableSensitiveDataLogging();
                });

            var persistenceConfigAction =
                EventSourcingPersistenceConfigAction(configuration.IsDevelopment(), domainEvents,
                    connectionString, EventSourcingDbContextConfig);

            services.ConfigureCommandWorker(configuration.IsDevelopment(), commandQueue,
                domainEvents,
                persistenceConfigAction,
                connectionString);
        }

        static void ConfigureRelationalPersistence(IServiceCollection services, IConfiguration configuration,
            string connectionString, PubSub<IDomainEvent> domainEvents, PubSub<ICommand> commandQueue)
        {
            void RelationalDbContextConfig(DbContextConfig<GleeViewModelContext> db) =>
                db.WithDbOptions(options =>
                {
                    if (configuration.IsDevelopment() || string.IsNullOrEmpty(connectionString))
                        options.UseSqlite("Data Source=relational_testing.db");
                    else
                        options.UseNpgsql(connectionString ?? throw new ArgumentNullException(nameof(connectionString)));

                    options.EnableSensitiveDataLogging();
                });

            var persistenceConfigAction = RelationalPersistenceConfigAction(new[] { typeof(Employee) },
                configuration.IsDevelopment(), domainEvents,
                connectionString, RelationalDbContextConfig);

            services.ConfigureCommandWorker(configuration.IsDevelopment(), commandQueue,
                domainEvents,
                persistenceConfigAction,
                connectionString);
        }

        static void ConfigureLocalCommandDispatcher(this IServiceCollection services,
            IPublisher<ICommand> commandQueue, Action<DbContextOptionsBuilder> viewModelDatabaseOptions)
        {
            services
                .AddLocalCommandDispatcher(commandQueue, config => config
                    .WithCommandValidatorsInAssemblies(typeof(VerifyNotDuplicate).Assembly,
                        typeof(ValidateCreateEmployee).Assembly))
                .WithPersistenceConfiguration(persistence =>
                    persistence
                        //EE24: START
                        .WithEntityTypes(typeof(EmployeeListing))
                        //EE24:END
                        //RP24: START
                        //.WithEntityTypes(typeof(Employee))
                        //RP24: END
                        .AddDbContext<GleeViewModelContext>(db => db.WithDbOptions(viewModelDatabaseOptions))
                        .WithGenericEntityRepository());
        }

        static void ConfigureCommandWorker(this IServiceCollection services,
            bool isDevelopment,
            ISubscriber<ICommand> commandQueue, IPublisher<IDomainEvent> domainEvents,
            Action<PersistenceConfig> persistenceConfig, string connectionString = "")
        {
            ((PubSub<IDomainEvent>)domainEvents).Subscribe(x => { Console.WriteLine("event", x.GetType()); });

            services.AddDispatchServer(commandQueue, config =>
            {
                config
                    .WithDependencies(HandlerDependencyBootstrapper.AddCommandHandlerDependencies())
                    .WithInitialization(p =>
                    {
                        var dbContext = p.GetService<IDbContext>();
                        //EE2:START LEAVE THIS CODE IF YOU WANT EVENT SOURCING (but remove the comment)
                        dbContext.Database.EnsureCreated();
                        //EE2:END
                        //RP2: START LEAVE THIS CODE IF YOU WANT RELATIONAL (but remove the other code and the conditional)
                        // dbContext.Database.Migrate();
                        //RP2: END
                    })
                    .WithCommandHandlersFrom(typeof(EmployeeCreator).Assembly)
                    .WithPersistenceConfiguration(persistenceConfig);
            });
        }

        static Action<PersistenceConfig> RelationalPersistenceConfigAction(Type[] entityTypes, bool isDevelopment,
            PubSub<IDomainEvent> domainEvents, string connectionString,
            Action<DbContextConfig<GleeViewModelContext>> dbContextConfigAction)

        {
            return persistence =>
            {
                persistence.WithEntityTypes(entityTypes)
                    .AddDbContext<GleeViewModelContext>(dbContextConfigAction)
                    .WithGenericEntityRepository(repo =>
                    {
                        repo.IncludeWritableRepository();
                    })
                    .WithDomainEventDispatch(domainEvents);
            };
        }

        static Action<PersistenceConfig> EventSourcingPersistenceConfigAction(bool isDevelopment,
            IPublisher<IDomainEvent> domainEvents, string connectionString,
            Action<DbContextConfig<EventSourcingDbContext>> dbContextConfigAction)
        {

            return persistence =>
                persistence
                    .WithWritableAggregateRootRepository<Employee>()
                    .AddDbContext<EventSourcingDbContext>(dbContextConfigAction
                    )
                    .WithDomainEventDispatch(domainEvents)
                    .WithCommandUnitOfWork();
        }

        static void ConfigureDomainEventWorker(this IServiceCollection services,
            ISubscriber<IDomainEvent> domainEvents, Action<DbContextOptionsBuilder> viewModelDatabaseOptions)
        {
            services.AddDispatchServer(domainEvents, config =>
            {
                config
                    .WithEventHandlersFrom(typeof(EmployeeListingEventHandlers).Assembly)
                    .WithPersistenceConfiguration(persistence =>
                    {
                        persistence
                            .WithEntityTypes(typeof(EmployeeListing))
                            .AddDbContext<GleeViewModelContext>(db => db.WithDbOptions(viewModelDatabaseOptions))
                            .WithGenericEntityRepository(x => x.IncludeWritableRepository())
                            .WithDomainEventUnitOfWork();
                    })
                    .WithDependencies(HandlerDependencyBootstrapper.AddEventHandlerDependencies())
                    .WithInitialization(p =>
                    {
                        var dbContext = p.GetService<IDbContext>();
                        dbContext.Database.Migrate();
                    });
            });
        }
    }
}