namespace Web.Infrastructure.Bootstrapping
{
    [Boilerplate]
    public class SwaggerConfiguration
    {
        /// <summary>
        /// <para>Glee2-dotnet API v1</para>
        /// </summary>
        public static string EndpointDescription => "Glee2-dotnet API v1";

        /// <summary>
        /// <para>/swagger/v1/swagger.json</para>
        /// </summary>
        public static string EndpointUrl => "/swagger/v1/swagger.json"; 

        /// <summary>
        /// <para>Acklen Avenue</para>
        /// </summary>
        public static string ContactName => "Acklen Avenue";

        /// <summary>
        /// <para>https://acklenavenue.com/</para>
        /// </summary>
        public static string ContactUrl => "https://acklenavenue.com/";

        /// <summary>
        /// <para>v1</para>
        /// </summary>
        public static string DocNameV1 => "v1";

        /// <summary>
        /// <para>Foo API</para>
        /// </summary>
        public static string DocInfoTitle => "Glee2-dotnet API";

        /// <summary>
        /// <para>v1</para>
        /// </summary>
        public static string DocInfoVersion => "v1";

        /// <summary>
        /// <para>Foo Api - Sample Web API in ASP.NET Core 2</para>
        /// </summary>
        public static string DocInfoDescription => "Glee2-dotnet Api - Sample Web API in ASP.NET Core 2";
    }
}