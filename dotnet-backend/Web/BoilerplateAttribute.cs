using System;

namespace Web
{
    [AttributeUsage(AttributeTargets.All)]
    public class BoilerplateAttribute : Attribute
    {
    }
}