using Web.Infrastructure;

namespace Web.Status
{
    public interface IApiInformationGetter
    {
        ApiInformation GetInfo();
    }
}