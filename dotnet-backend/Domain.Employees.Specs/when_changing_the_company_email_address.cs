using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_the_company_email_address
    {
        static Employee _systemUnderTest;
        static string _newCompanyEmail;

        Establish _context = () =>
        {
            _newCompanyEmail = "new company email";
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _systemUnderTest.ChangeCompanyEmail(_newCompanyEmail); };

        It should_change_the_address = () => { _systemUnderTest.CompanyEmail.Should().Be(_newCompanyEmail); };

        It should_notify_the_world_of_the_change = () =>
            _systemUnderTest.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeCompanyEmailChanged(_systemUnderTest.Id, _newCompanyEmail));
    }
}