using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_the_EffectiveDate
    {
        static Employee _systemUnderTest;
        static DateTime _newEffectiveDate;

        Establish _context = () =>
        {
            _newEffectiveDate = DateTime.Now;
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _systemUnderTest.ChangeEffectiveDate(_newEffectiveDate); };

        It should_change_the_effective_date = () => { _systemUnderTest.EffectiveDate.Should().Be(_newEffectiveDate); };

        It should_notify_the_world_of_the_change = () =>
            _systemUnderTest.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeEffectiveDateChanged(_systemUnderTest.Id, _newEffectiveDate));
    }
}