using System;
using Avenue.Domain;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Handlers;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Handlers
{
    public class when_changing_an_employees_phone_number
    {
        const string NewPhoneNumber = "new Phone number";
        static EmployeePhoneNumberChanger _systemUnderTest;
        static Guid _employeeId;
        static Employee _employee;
        static IWritableRepository<Employee> _repo;

        Establish _context = () =>
        {
            _employeeId = Guid.NewGuid();
            _employee = Mock.Of<Employee>();

            _repo = Mock.Of<IWritableRepository<Employee>>();

            Mock.Get(_repo).Setup(x => x.Find(Moq.It.Is<Guid>(id => id == _employeeId)))
                .ReturnsAsync(_employee);

            _systemUnderTest = new EmployeePhoneNumberChanger(_repo);
        };

        Because of = () => _systemUnderTest.Handle(new ChangeEmployeePhoneNumber(_employeeId, NewPhoneNumber)).Await();

        It should_change_the_phone_number = () =>
            Mock.Get(_employee).Verify(x => x.ChangePhoneNumber(NewPhoneNumber));

        It should_persist_the_changes = () => Mock.Get(_repo).Verify(x => x.Update(_employee));
    }
}