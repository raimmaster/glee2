using System;
using Avenue.Domain;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Handlers;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Handlers
{
    public class when_changing_an_employees_address
    {
        static EmployeeAddressChanger _systemUnderTest;
        static IWritableRepository<Employee> _writableRepository;
        static Guid _employeeId;
        static string _newAddress;
        static string _newCountry;
        static string _newCity;
        static string _newRegion;
        static Employee _employee;

        Establish context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee>>();
            _systemUnderTest = new EmployeeAddressChanger(_writableRepository);

            _employeeId = Guid.NewGuid();
            _newAddress = "Barrio La Tigra";
            _newCountry = "Honduras";
            _newCity = "Tegucigalpa";
            _newRegion = "Centro";

            _employee = Mock.Of<Employee>();
            Mock.Get(_writableRepository).Setup(x => x.Find(Moq.It.Is<Guid>(i => i == _employeeId)))
                .ReturnsAsync(_employee);
        };

        Because of = () =>
        {
            _systemUnderTest
                .Handle(new ChangeEmployeeAddress(_employeeId, _newAddress, _newCity, _newRegion, _newCountry))
                .Await();
        };

        It should_change_the_address_info = () =>
        {
            Mock.Get(_employee).Verify(x =>
                x.ChangeAddress(_newAddress, _newCity, _newRegion, _newCountry));
        };

        It should_save_the_employee_changes = () => { Mock.Get(_writableRepository).Verify(x => x.Update(_employee)); };
    }
}