using System;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Validators.ForCreateEmployee;
using FizzWare.NBuilder;
using Machine.Specifications;
using Testing.Utilities;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Validators
{
    public class when_validating_a_create_employee_command
    {
        static ValidateCreateEmployee _validator;

        Establish context = () =>
        {
            _validator = new ValidateCreateEmployee();
            _command = Builder<CreateEmployee>.CreateNew()
                .With(employee => employee.FirstName, "")
                .With(e => e.LastName, "")
                .With(e => e.CompanyEmail, "")
                .With(e => e.EffectiveDate, default)
                .With(e => e.Gender, "")
                .With(e => e.StartDate, default)
                .With(e => e.SalaryType, "")
                .With(e => e.Salary, 0)
                .With(e => e.Country, "")
                .With(e => e.Region, "")
                .With(e => e.City, "")
                .Build();
        
        };

        Because of = () => _result = Catch.Exception(() => _validator.Validate(_command).Await());

        It should_return_errors_for_first_name = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "FirstName");

        It should_return_errors_for_last_name = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "LastName");

        It should_return_errors_for_email = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "CompanyEmail");
        
        It should_return_errors_for_effective_date = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "EffectiveDate");
        
        It should_return_errors_for_gender = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "Gender");
        
        It should_return_errors_for_start_date = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "StartDate");
        
        It should_return_errors_for_salary_type = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "SalaryType");
        
        It should_return_errors_for_salary = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "Salary");
        
        It should_return_errors_for_country = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "Country");
        
        It should_return_errors_for_region = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "Region");
        
        It should_return_errors_for_city = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "City");

        static CreateEmployee _command;
        static Exception _result;
    }
}