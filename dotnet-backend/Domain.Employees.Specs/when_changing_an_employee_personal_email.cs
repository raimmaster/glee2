using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_an_employee_personal_email
    {
        static Employee _employee;
        static string _newPersonalEmail;

        Establish context = () =>
        {
            _newPersonalEmail = "new personal email";
            _employee = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .With(x => x.PersonalEmail, "myemail@google.com").Build();
        };

        Because of = () => { _employee.ChangePersonalEmail(_newPersonalEmail); };

        It should_change_the_personal_email = () => { _employee.PersonalEmail.Should().Be(_newPersonalEmail); };

        It should_publish_update_event = () =>
        {
            _employee.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeePersonalEmailChanged(_employee.Id, _newPersonalEmail));
        };
    }
}