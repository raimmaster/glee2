using System;
using System.Threading.Tasks;
using Domain.Commands;
using Domain.Commands.Handlers;
using Domain.Entities;
using Domain.Repositories;
using Domain.Specs.Commands.Helpers;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Specs.Commands.Handlers
{
    [Subject(typeof(EmployeeRemover))]
    public class when_handling_a_delete_employee_command
    {
        static IWritableRepository<Employee, Guid> _writableRepository;
        static EmployeeRemover _handler;
        static RemoveEmployee _removeEmployee;
        static Employee _employee;

        Establish context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee, Guid>>();

            _handler = new EmployeeRemover(_writableRepository);

            _removeEmployee = Builder<RemoveEmployee>.CreateNew().Build();

            _employee = Mock.Of<Employee>();

            Mock.Get(_writableRepository).Setup(x => x.GetById(Moq.It.IsAny<Guid>()))
                .Returns(Task.FromResult(_employee));
        };

        Because of = () => _handler.Handle(_removeEmployee).Await();


        It should_delete_the_employee_in_the_db = () =>
        {
            Mock.Get(_writableRepository).Verify(repository =>
                repository.Update(Moq.It.Is<Employee>(employee => _employee.ShouldBeEquivalent(employee))));
        };

        It should_tell_the_employee_to_remove_itself = () => { Mock.Get(_employee).Verify(x => x.Remove()); };
    }
}