using System;
using Domain.Commands;
using Domain.Commands.Handlers;
using Domain.Entities;
using Domain.Repositories;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Specs.Commands.Handlers
{
    public class when_changing_a_company_email
    {
        static EmployeeCompanyEmailChanger _systemUnderTest;
        static IWritableRepository<Employee, Guid> _repo;
        static Guid _employeeId;
        static string _newCompanyEmail;
        static Employee _employee;

        Establish _context = () =>
        {
            _employeeId = Guid.NewGuid();
            _newCompanyEmail = "new company email";

            _repo = Mock.Of<IWritableRepository<Employee, Guid>>();

            _employee = Mock.Of<Employee>();
            Mock.Get(_repo).Setup(x => x.GetById(Moq.It.Is<Guid>(id => id == _employeeId))).ReturnsAsync(_employee);

            _systemUnderTest = new EmployeeCompanyEmailChanger(_repo);
        };

        Because of = () =>
        {
            _systemUnderTest.Handle(new ChangeEmployeeCompanyEmail(_employeeId, _newCompanyEmail)).Await();
        };

        It should_change_the_email = () => { Mock.Get(_employee).Verify(x => x.ChangeCompanyEmail(_newCompanyEmail)); };

        It should_persist_the_change = () => { Mock.Get(_repo).Verify(x => x.Update(_employee)); };
    }
}