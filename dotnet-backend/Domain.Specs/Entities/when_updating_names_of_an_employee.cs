using System;
using System.Linq;
using Domain.Entities;
using Domain.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Specs.Entities
{
    public class when_updating_names_of_an_employee
    {
        static Employee _employee;

        Establish context = () =>
        {
            _employee = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _employee.ChangeNames("1", "2", "3", "4"); };

        It should_change_the_names = () =>
        {
            _employee.FirstName.Should().Be("1");
            _employee.MiddleName.Should().Be("2");
            _employee.LastName.Should().Be("3");
            _employee.SecondLastName.Should().Be("4");
        };

        It should_publish_update_event = () =>
        {
            _employee.GetChanges().First().Should()
                .BeEquivalentTo(new EmployeeNamesChanged(_employee.Id, "1", "2", "3", "4"));
        };
    }
}