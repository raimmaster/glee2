using System;
using Domain.Employees.Commands;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Requests;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_changing_employee_personal_email_address : given_an_employee_controller_context
    {
        static UpdateEmployeeRequest _updateEmployeeRequest;
        static Guid _id;

        Establish context = () =>
        {
            _id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e");

            _updateEmployeeRequest = Builder<UpdateEmployeeRequest>.CreateNew()
                .Build();
        };

        Because of = () => { _employeesController.ChangePersonalEmail(_id, _updateEmployeeRequest).Await(); };

        It should_dispatch_update_employee_personal_email_address_command = () =>
            Mock.Get(_commandDispatcher).ShouldDispatchACommandLike<ChangeEmployeePersonalEmail>(x =>
                x.Id == _id &&
                x.PersonalEmail == _updateEmployeeRequest.PersonalEmail);
    }
}