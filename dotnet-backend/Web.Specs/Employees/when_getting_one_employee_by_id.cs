using System;
using Data.Projections;
using Domain.Employees;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Web.Employees;
using Web.Employees.Responses;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_getting_one_employee_by_id : given_an_employee_controller_context
    {
        static Guid _id;

        Establish _context = () =>
        {
            _id = Guid.NewGuid();

            //EE22: START
            var employee = Builder<EmployeeListing>.CreateNew()
                .With(x => x.Id, _id).Build();
            //EE22: END
            //RP22: START
            // var employee = Builder<Employee>.CreateNew()
            //     .With(x => x.Id, _id).Build();
            //RP22: END

            Mock.Get(_readOnlyRepository).Setup(x => x.GetById(Moq.It.Is<Guid>(i => i == _id))).ReturnsAsync(employee);

            _expectedResponse = employee;
        };

        Because of = async () => { _result = await _employeesController.GetOne(_id); };

        It should_return_the_matching_employee = () => { _result.Should().Be(_expectedResponse); };
        //EE23: START
        static EmployeeListing _expectedResponse;
        static EmployeeListing _result;
        //EE23: END
        //RP23: START
        // static Employee _expectedResponse;
        // static Employee _result;
        //RP23: END
    }
}