using System;
using AutoMapper;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Data.Projections;
using Domain.Common.Services;
using Domain.Employees.Commands;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using It = Machine.Specifications.It;
using ItVerify = Moq.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_an_employee_is_deleted : given_an_employee_controller_context
    {
        static RemoveEmployee _expectedCommand;
        static Guid _employeeId;

        Establish context = () =>
        {
            _employeeId = Guid.NewGuid();

            _expectedCommand = new RemoveEmployee(_employeeId);
        };

        Because of = () => { _employeesController.DeleteEmployee(_employeeId).Await(); };

        It should_dispatch_delete_employee_command = () =>
        {
            Mock.Get(_commandDispatcher).Verify(dispatcher =>
                dispatcher.Dispatch(ItVerify.Is<RemoveEmployee>(deleteEmployeeCommand =>
                    _expectedCommand.ShouldBeEquivalent(deleteEmployeeCommand))));
        };
    }
}