using Common.Commands.Validators;
using FluentValidation;

namespace Domain.Commands.Validators.ForCreateEmployee
{
    public class CheckProperties : FluentValidationComponent<CreateEmployee>
    {
        public CheckProperties()
        {
            RuleFor(employee => employee.FirstName).NotNull();

        }
    }
}