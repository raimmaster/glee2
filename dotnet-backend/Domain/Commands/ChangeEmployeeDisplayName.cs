using System;

namespace Domain.Commands
{
    public class ChangeEmployeeDisplayName
    {
        public Guid Id { get; protected set; }
        public string DisplayName { get; protected set; }

        public ChangeEmployeeDisplayName(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }

        protected ChangeEmployeeDisplayName()
        {
        }
    }
}