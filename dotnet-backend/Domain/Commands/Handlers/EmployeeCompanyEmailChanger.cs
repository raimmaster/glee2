using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeeCompanyEmailChanger:ICommandHandler<ChangeEmployeeCompanyEmail>
    {
        readonly IWritableRepository<Employee, Guid> _repo;

        public EmployeeCompanyEmailChanger(IWritableRepository<Employee,Guid> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeCompanyEmail command)
        {
            var employee = await _repo.GetById(command.Id);
            employee.ChangeCompanyEmail(command.Email);
            await _repo.Update(employee);
        }
    }
}