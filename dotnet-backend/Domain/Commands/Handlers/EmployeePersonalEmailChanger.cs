using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeePersonalEmailChanger : ICommandHandler<ChangeEmployeePersonalEmail>
    {
        private readonly IWritableRepository<Employee, Guid> _repo;

        public EmployeePersonalEmailChanger(IWritableRepository<Employee,Guid> repo)
        {
            _repo = repo;
        }


        public async Task Handle(ChangeEmployeePersonalEmail command)
        {
            var employee = await _repo.GetById(command.Id);
            employee.ChangePersonalEmail(command.PersonalEmail);
            await _repo.Update(employee);
        }
    }
}