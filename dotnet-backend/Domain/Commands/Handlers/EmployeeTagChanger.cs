using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeeTagChanger: ICommandHandler<ChangeEmployeeTags>
    {
        readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeeTagChanger(IWritableRepository<Employee,Guid> writableRepository)
        {
            _writableRepository = writableRepository;          
        }

        public async Task Handle(ChangeEmployeeTags command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.ChangeTags(command.Id, command.Tags);
            await _writableRepository.Update(employee);
        }
    }
}