using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IReadOnlyRepository
    {
        
    }
    public interface IReadOnlyRepository<T, TPKey> : IReadOnlyRepository
    {
        Task<IEnumerable<T>> FindAll();
        Task<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> expression, bool allowRemoved = false);
        Task<T> GetById(TPKey id);
    }
}