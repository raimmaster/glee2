using System;
using Common.Entities;

namespace Domain.Events
{
    public class EmployeePhoneNumberChanged : IEvent
    {
        public Guid Id { get; }
        public string NewPhoneNumber { get; }

        public EmployeePhoneNumberChanged(Guid id, string newPhoneNumber)
        {
            Id = id;
            NewPhoneNumber = newPhoneNumber;
        }
    }
}