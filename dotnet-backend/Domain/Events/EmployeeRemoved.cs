using System;
using Common.Entities;

namespace Domain.Events
{
    public class EmployeeRemoved : IEvent
    {
        public Guid EmployeeId { get; }

        public EmployeeRemoved(Guid employeeId)
        {
            EmployeeId = employeeId;
        }

    }
}