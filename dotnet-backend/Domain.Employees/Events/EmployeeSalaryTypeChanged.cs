using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeSalaryTypeChanged : IDomainEvent
    {
        protected EmployeeSalaryTypeChanged()
        {
            
        }

        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public string SalaryType { get; set; } = "";

        public EmployeeSalaryTypeChanged(Guid id, string salaryType)
        {
            EmployeeId = id;
            SalaryType = salaryType;
        }
    }
}