using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeSalaryChanged : IDomainEvent
    {
        protected EmployeeSalaryChanged()
        {
            
        }

        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public double Salary { get; set; } = 0.0;

        public EmployeeSalaryChanged(Guid id, double salary)
        {
            EmployeeId = id;
            Salary = salary;
        }
    }
}