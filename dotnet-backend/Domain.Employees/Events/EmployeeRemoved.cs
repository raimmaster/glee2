using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeRemoved : IDomainEvent
    {
        protected EmployeeRemoved()
        {
            
        }
        public Guid EmployeeId { get; protected set; } = Guid.Empty;

        public EmployeeRemoved(Guid employeeId)
        {
            EmployeeId = employeeId;
        }
    }
}