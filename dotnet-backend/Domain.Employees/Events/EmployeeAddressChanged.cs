using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeAddressChanged : IDomainEvent
    {
        protected EmployeeAddressChanged()
        {
            
        }

        public Guid EmployeeId
        {
            get;
            protected set;
        } = Guid.Empty;
        public string Address { get; } = "";
        public string City { get; } = "";
        public string Region { get; } = "";
        public string Country { get; } = "";


        public EmployeeAddressChanged(Guid employeeId, string address, string city, string region, string country)
        {
            EmployeeId = employeeId;
            Address = address;
            City = city;
            Region = region;
            Country = country;
        }
    }
}