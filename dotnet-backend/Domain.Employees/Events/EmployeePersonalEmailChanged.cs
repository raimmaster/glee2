using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeePersonalEmailChanged : IDomainEvent
    {
        protected EmployeePersonalEmailChanged()
        {
            
        }
        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public string PersonalEmail { get; } = "";

        public EmployeePersonalEmailChanged(Guid employeeId, string personalEmail)
        {
            EmployeeId = employeeId;
            PersonalEmail = personalEmail;
        }
    }
}