using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeEffectiveDate : ICommand
    {
        public Guid Id { get; }
        public DateTime EffectiveDate { get; }

        public ChangeEmployeeEffectiveDate(Guid id, DateTime effectiveDate)
        {
            Id = id;
            EffectiveDate = effectiveDate;
        }
    }
}