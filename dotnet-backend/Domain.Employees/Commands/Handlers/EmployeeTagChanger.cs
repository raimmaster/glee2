using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeTagChanger : ICommandHandler<ChangeEmployeeTags>
    {
        readonly IWritableRepository<Employee> _writableRepository;

        public EmployeeTagChanger(IWritableRepository<Employee> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeeTags command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _writableRepository.Find(command.Id);
            employee.ChangeTags(command.Tags);
            await _writableRepository.Update(employee);
        }
    }
}