using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeEffectiveDateChanger : ICommandHandler<ChangeEmployeeEffectiveDate>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeeEffectiveDateChanger(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeEffectiveDate command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangeEffectiveDate(command.EffectiveDate);
            await _repo.Update(employee);
        }
    }
}