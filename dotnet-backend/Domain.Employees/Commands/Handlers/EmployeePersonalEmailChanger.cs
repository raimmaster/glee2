using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeePersonalEmailChanger : ICommandHandler<ChangeEmployeePersonalEmail>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeePersonalEmailChanger(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }


        public async Task Handle(ChangeEmployeePersonalEmail command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangePersonalEmail(command.PersonalEmail);
            await _repo.Update(employee);
        }
    }
}