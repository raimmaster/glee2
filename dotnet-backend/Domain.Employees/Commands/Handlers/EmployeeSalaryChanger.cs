using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeSalaryChanger : ICommandHandler<ChangeEmployeeSalary>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeeSalaryChanger(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeSalary command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangeSalary(command.Salary);
            await _repo.Update(employee);
        }
    }
}