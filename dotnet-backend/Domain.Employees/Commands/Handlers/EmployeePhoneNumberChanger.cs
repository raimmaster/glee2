using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeePhoneNumberChanger : ICommandHandler<ChangeEmployeePhoneNumber>
    {
        readonly IWritableRepository<Employee> _writableRepository;

        public EmployeePhoneNumberChanger(IWritableRepository<Employee> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeePhoneNumber command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _writableRepository.Find(command.Id);
            employee.ChangePhoneNumber(command.PhoneNumber);

            await _writableRepository.Update(employee);
        }
    }
}