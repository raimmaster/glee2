using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeSalaryTypeChanger : ICommandHandler<ChangeEmployeeSalaryType>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeeSalaryTypeChanger(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeSalaryType command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangeSalaryType(command.SalaryType);
            await _repo.Update(employee);
        }
    }
}