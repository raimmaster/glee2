using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeCompanyEmailChanger : ICommandHandler<ChangeEmployeeCompanyEmail>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeeCompanyEmailChanger(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeCompanyEmail command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangeCompanyEmail(command.CompanyEmail);
            await _repo.Update(employee);
        }
    }
}