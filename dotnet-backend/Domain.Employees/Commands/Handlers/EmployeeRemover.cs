using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeRemover : ICommandHandler<RemoveEmployee>
    {
        readonly IWritableRepository<Employee> _writableRepository;

        public EmployeeRemover(IWritableRepository<Employee> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(RemoveEmployee command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _writableRepository.Find(command.Id);
            employee.Remove();
            await _writableRepository.Update(employee);
        }
    }
}