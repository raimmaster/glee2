using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeIsActive : ICommand
    {
        public Guid Id { get; }
        public bool IsActive { get; }

        public ChangeEmployeeIsActive(Guid id, bool isActive)
        {
            Id = id;
            IsActive = isActive;
        }
    }
}