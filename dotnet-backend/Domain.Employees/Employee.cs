using System;
using System.Collections.Generic;
using Avenue.Domain;
using Avenue.EventSourcing;
using Domain.Common;
using Domain.Employees.Events;

namespace Domain.Employees
{
    public class Employee : AggregateRoot, IRemovable, Avenue.EventSourcing.IAggregateRoot
    {
        protected Employee() : base(new List<object>())
        {
        }

        public Employee(IEnumerable<object> events) : base(events)
        {
        }

        public Employee(Guid id, string firstName, string? middleName, string lastName, string? secondLastName,
            string displayName,
            string email, string? personalEmail, DateTime birthDate, DateTime? startDate, string address,
            string phoneNumber, string bankName, string accountNumber, string gender, string tags, string country,
            string region, string city, double? salary, DateTime? effectiveDate, string? salaryType) : base(
            new List<object>())
        {
            When(NewChange(new EmployeeCreated(id, firstName, middleName, lastName, secondLastName, displayName, email,
                personalEmail, birthDate, startDate
                , address, phoneNumber, bankName, accountNumber, gender, tags, country, region, city, salary,
                effectiveDate, salaryType)));
        }

        void When(EmployeeCreated @event)
        {
            Id = @event.EmployeeId;
            FirstName = @event.FirstName;
            MiddleName = @event.MiddleName;
            LastName = @event.LastName;
            SecondLastName = @event.SecondLastName;
            DisplayName = @event.DisplayName;
            CompanyEmail = @event.CompanyEmail;
            PersonalEmail = @event.PersonalEmail;
            Birthdate = @event.Birthdate;
            StartDate = @event.StartDate;
            Address = @event.Address;
            PhoneNumber = @event.PhoneNumber;
            BankName = @event.BankName;
            AccountNumber = @event.AccountNumber;
            Gender = @event.Gender;
            Tags = @event.Tags;
            Country = @event.Country;
            Region = @event.Region;
            City = @event.City;
            Salary = @event.Salary;
            EffectiveDate = @event.EffectiveDate;
            SalaryType = @event.SalaryType;
            IsActive = true;
        }

        public virtual string FirstName { get; protected set; } = "";
        public virtual string? MiddleName { get; protected set; }
        public virtual string LastName { get; protected set; } = "";
        public virtual string? SecondLastName { get; protected set; }
        public virtual string DisplayName { get; protected set; } = "";
        public virtual string CompanyEmail { get; protected set; } = "";
        public virtual string? PersonalEmail { get; protected set; }
        public virtual DateTime Birthdate { get; protected set; } = DateTime.MinValue;
        public virtual DateTime? StartDate { get; protected set; } = DateTime.MinValue;
        public virtual string Address { get; protected set; } = "";
        public virtual string PhoneNumber { get; protected set; } = "";
        public virtual string BankName { get; protected set; } = "";
        public virtual string AccountNumber { get; protected set; } = "";
        public virtual string Gender { get; protected set; } = "";
        public virtual string Tags { get; protected set; } = "";
        public virtual string Country { get; protected set; } = "";
        public virtual string Region { get; protected set; } = "";
        public virtual string City { get; protected set; } = "";
        public virtual double? Salary { get; protected set; } = 0;
        public virtual DateTime? EffectiveDate { get; protected set; } = DateTime.MinValue;
        public virtual string? SalaryType { get; protected set; } = "";
        public virtual bool IsActive { get; protected set; } = true;

        public virtual void Remove()
        {
            When(NewChange(new EmployeeRemoved(Id)));
        }

        void When(EmployeeRemoved @event)
        {
            Removed = true;
        }

        public virtual void ChangeNames(string firstName, string middleName, string lastName, string secondLastName)
        {
            When(NewChange(new EmployeeNamesChanged(Id, firstName, middleName, lastName, secondLastName)));
        }

        void When(EmployeeNamesChanged @event)
        {
            FirstName = @event.FirstName;
            MiddleName = @event.MiddleName;
            LastName = @event.LastName;
            SecondLastName = @event.SecondLastName;
        }

        public virtual void ChangeTags(string newTags)
        {
            When(NewChange(new EmployeeTagsChanged(Id, newTags)));
        }

        void When(EmployeeTagsChanged @event)
        {
            Tags = @event.Tags;
        }

        public virtual void ChangeAddress(string newAddress, string newCity, string newRegion, string newCountry)
        {
            When(NewChange(new EmployeeAddressChanged(Id, newAddress, newCity, newRegion, newCountry)));
        }

        void When(EmployeeAddressChanged @event)
        {
            Address = @event.Address;
            City = @event.City;
            Region = @event.Region;
            Country = @event.Country;
        }

        public virtual void ChangeDisplayName(string newDisplayName)
        {
            When(NewChange(new EmployeeDisplayNameChanged(Id, newDisplayName)));
        }

        void When(EmployeeDisplayNameChanged @event)
        {
            DisplayName = @event.DisplayName;
        }

        public virtual void ChangePhoneNumber(string newPhoneNumber)
        {
            When(NewChange(new EmployeePhoneNumberChanged(Id, newPhoneNumber)));
        }

        void When(EmployeePhoneNumberChanged @event)
        {
            PhoneNumber = @event.PhoneNumber;
        }

        public virtual void ChangePersonalEmail(string newPersonalEmail)
        {
            When(NewChange(new EmployeePersonalEmailChanged(Id, newPersonalEmail)));
        }

        void When(EmployeePersonalEmailChanged @event)
        {
            PersonalEmail = @event.PersonalEmail;
        }

        public virtual void ChangeCompanyEmail(string newCompanyEmail)
        {
            When(NewChange(new EmployeeCompanyEmailChanged(Id, newCompanyEmail)));
        }
        void When(EmployeeCompanyEmailChanged @event)
        {
            CompanyEmail = @event.CompanyEmail;
        }

        public virtual void ChangeSalary(double newSalary)
        {
            When(NewChange(new EmployeeSalaryChanged(Id, newSalary)));
        }
        void When(EmployeeSalaryChanged @event) {Salary = @event.Salary; }
        
        public virtual void ChangeSalaryType(string newSalaryType){
            When(NewChange(new EmployeeSalaryTypeChanged(Id, newSalaryType)));
        }
        void When(EmployeeSalaryTypeChanged @event) {SalaryType = @event.SalaryType; }
        public virtual void ChangeEffectiveDate(DateTime newEffectiveDate)
        {
            When(NewChange(new EmployeeEffectiveDateChanged(Id, newEffectiveDate)));
        }
        void When(EmployeeEffectiveDateChanged @event) {EffectiveDate = @event.EffectiveDate; }
        public virtual void ChangeBirthDate(DateTime newBirthDate)
        {
            When(NewChange(new EmployeeBirthDateChanged(Id, newBirthDate)));
        }
        void When(EmployeeBirthDateChanged @event) {Birthdate = @event.Birthdate; }

        public virtual bool Removed { get; protected set; }

        public virtual void ChangeIsActive(bool isActive)
        {
            When(NewChange(new EmployeeIsActiveChanged(Id, isActive)));
        }
        void When(EmployeeIsActiveChanged @event)
        {
            IsActive = @event.IsActive;
        }
    }
}