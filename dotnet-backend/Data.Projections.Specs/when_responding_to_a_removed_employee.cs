using System;
using Avenue.Testing.Moq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class when_responding_to_a_removed_employee : given_an_employee_listing_event_handler_context
    {
        Establish _context = () =>
        {
            _employeeId = Guid.NewGuid();
            _event = Builder<EmployeeRemoved>.CreateNew()
                .With(x=> x.EmployeeId, _employeeId)
                .Build();

            var employeeListing = Builder<EmployeeListing>.CreateNew().Build();
            Mock.Get(_writableRepository).Setup(x => x.Find(_employeeId)).ReturnsAsync(employeeListing);
        };

        Because of = () => { _systemUnderTest.Handle(_event).Wait(); };

        It should_update_the_display_name_in_the_repo = () =>
        {
            Mock.Get(_writableRepository).Verify(x =>
                x.Update(WithEventHandlersFromSome<EmployeeListing>.With(e => e.Removed)));
        };

        static EmployeeRemoved _event;
        static Guid _employeeId;
    }
}