using System.Threading.Tasks;
using Avenue.Commands;
using FluentValidation;

namespace Domain.Common
{
    public abstract class CommandValidatorBase<TCommand> : AbstractValidator<TCommand>, ICommandValidator<TCommand>
    {
        public new async Task Validate(TCommand command)
        {
            var result = await ValidateAsync(command);
            if (!result.IsValid) 
                throw new CommandValidationException<TCommand>(result.Errors);
        }
    }
}