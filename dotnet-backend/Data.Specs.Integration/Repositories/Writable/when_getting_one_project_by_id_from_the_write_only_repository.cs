using System;
using System.Linq;
using AcklenAvenue.Events;
using Data.Repositories;
using Domain.Employees;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Specs.Integration.Repositories.Writable
{
    public class when_getting_one_project_by_id_from_the_write_only_repository
    {
        static ReadOnlyRepository<Employee, Guid> _systemUnderTest;
        static Employee _result;
        static Employee _selectedEmployee;
        static IEventDispatcher _eventDispatcher;
        static AppDataContext _appDataContext;

        Cleanup after = () => { _appDataContext.Clean(); };

        Establish context = () =>
        {
            _appDataContext = InMemoryDataContext
                .GetInMemoryContext()
                .Seed();

            _selectedEmployee = _appDataContext.Set<Employee>().ToList()[2];
            _eventDispatcher = Mock.Of<IEventDispatcher>();
            _systemUnderTest = new ReadOnlyRepository<Employee, Guid>(_appDataContext);
        };

        Because of = async () => { _result = await _systemUnderTest.GetById(_selectedEmployee.Id); };

        It should_return_the_selected_project = () =>
            _result.Should().BeEquivalentTo(_selectedEmployee);
    }
}