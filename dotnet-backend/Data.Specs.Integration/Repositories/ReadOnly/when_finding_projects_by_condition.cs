using System;
using System.Collections.Generic;
using System.Linq;
using Data.Repositories;
using Domain.Employees;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Specs.Integration.Repositories.ReadOnly
{
    public class when_finding_projects_by_condition
    {
        static ReadOnlyRepository<Employee, Guid> _systemUnderTest;
        static IEnumerable<Employee> _result;
        static string _lastName;
        static Employee _expectedEmployee;
        static AppDataContext _appDataContext;

        Establish _context = () =>
        {
            _appDataContext = InMemoryDataContext.GetInMemoryContext().Seed();

            _systemUnderTest = new ReadOnlyRepository<Employee, Guid>(_appDataContext);

            _expectedEmployee = _appDataContext.Set<Employee>().First();

            _lastName = _expectedEmployee.LastName;
        };

        Cleanup after = () => { _appDataContext.Clean(); };

        Because of = async () => { _result = await _systemUnderTest.FindByCondition(x => x.LastName == _lastName); };

        It should_return_the_matching_projects = () => { _result.Should().ContainEquivalentOf(_expectedEmployee); };
    }
}