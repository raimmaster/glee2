using System;

namespace Data.Projections.ExceptionHandling
{
    public class EventNullException : Exception
    {
        public EventNullException(string message) : base(message)
        {}
    }
}