using System;
using Avenue.Domain;

namespace Data.Projections
{
    public interface IProjection : IRemovable
    {
        Guid Id { get; }
    }
}