using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AcklenAvenue.Events;
using Common.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class WritableRepository<T, TPKey> : IWritableRepository<T, TPKey> where T : class, IAggregate
    {
        private readonly IEventDispatcher _dispatcher;
        private AppDataContext Context { get; set; }

        public WritableRepository(AppDataContext context, IEventDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
            Context = context;
        }
        
        private async Task PublishEvents(T entity)
        {
            var events = entity.GetChanges().ToList();

            foreach (var domainEvent in events)
            {
                await _dispatcher.Dispatch(domainEvent);
            }
        }
        
        public async Task<T> Create(T entity)
        {
            Context.Set<T>().Add(entity);
            await Context.SaveChangesAsync();

            await PublishEvents(entity);

            return entity;
        }

        public async Task<T> Update(T entity)
        {
            Context.Set<T>().Update(entity);

            await Context.SaveChangesAsync();
            await PublishEvents(entity);
            return entity;
        }

        public async Task<IEnumerable<T>> FindAll()
        {
            return await Context.Set<T>()
                .Where(x=> !x.Removed)
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> expression, bool allowRemoved = false)
        {
            var queryable = Context.Set<T>().Where(expression);
            
            if (!allowRemoved)
                queryable = queryable.Where(x => !x.Removed);
            
            return await queryable.ToListAsync();
        }

        public Task<T> GetById(TPKey id)
        {
            var entity =  Context.Set<T>().FindAsync(id);
            return entity;
        }
    }
}