namespace Common.Entities
{
    public interface IEntity
    {
        
    }
    public interface IEntity<TPKey> : IEntity
    {
        TPKey Id { get; set; }
    }
}