using System.Collections.Generic;
using System.Linq;

namespace Common.Commands.Validators
{
    public interface IValidationResult
    {
        bool HasErrors { get; }
        IEnumerable<ValidationError> Errors { get; }
    }

    public class CommandValidationResult : IValidationResult
    {
        public CommandValidationResult( IEnumerable<ValidationError> errors)
        {
            var validationErrors = errors as ValidationError[] ?? errors.ToArray();
            HasErrors = validationErrors.Any();
            Errors = validationErrors;
        }

        public bool HasErrors { get; }
        public IEnumerable<ValidationError> Errors { get; }
    }
}