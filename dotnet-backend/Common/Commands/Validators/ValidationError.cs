namespace Common.Commands.Validators
{
    [Boilerplate]
    public class ValidationError
    {
        public string Field { get; set; }
        public string FieldLabel { get; set; }
        public string Message { get; set; }
        public object Value { get; set; }
    }
}