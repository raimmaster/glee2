#tool nuget:?package=NUnit.ConsoleRunner&version=3.4.0
//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////
 
var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
 
 
//////////////////////////////////////////////////////////////////////
///    Build Variables
/////////////////////////////////////////////////////////////////////
var binDir = "bin";       // Destination Binary File Directory name i.e. bin
var projJson = "";     // Path to the project.json
var projDir = ".";      //  Project Directory
var solutionFile = "glee.sln"; // Solution file if needed
var outputDir = Directory("build") + Directory(configuration);  // The output directory the build artefacts saved too
 
//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////
 
Task("Clean")
    .Does(() =>
{
    if (DirectoryExists(outputDir))
        {
            var deleteSettings = new DeleteDirectorySettings{
                Recursive=true
            };
            DeleteDirectory(outputDir, deleteSettings);
        }
});
 
Task("Restore")
    .Does(() => {
        var settings = new DotNetCoreRestoreSettings
        {
        };
        DotNetCoreRestore(solutionFile);
    });
 
Task("Build")
    .IsDependentOn("Clean")
    .IsDependentOn("Restore")
    .Does(() => {        
        var buildSettings = new DotNetCoreBuildSettings
        {
            //Framework = "netcoreapp2.2",
            Configuration = "Release",
            OutputDirectory = outputDir
        };
	   DotNetCoreBuild(solutionFile, buildSettings);
    });
 
Task("Package")
    .IsDependentOn("Build")
    .Does(() => {
        var packSettings = new DotNetCorePackSettings
        {
            OutputDirectory = outputDir,
            NoBuild = true
        };
 
         DotNetCorePack(solutionFile, packSettings);
 
    });
 
 Task("Run")
    .IsDependentOn("Build")
    .Does(() => {
         DotNetCoreRun("./Web");
    });
//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////
 
Task("Default")
    .IsDependentOn("Package");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////
 
RunTarget(target);