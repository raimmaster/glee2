Feature: retrieve employee
    In order to \_____ as a \______ I can \______.

  @Regression
  Scenario: Get Employees by Id (uid:94a624b5-d6ce-415e-83cb-0d278dc1a29f)
    Given an active employee
    When I retrieve an employee by the Id
    Then I should get the same employee

  @Regression
  Scenario: Find Employees by Last Name (uid:db7a963c-cc8e-4571-86e0-3275f106e6b5)
    Given an active employee
    And the employee's name is "Jeff Alln Johnson William"
    And an active employee
    And the employee's name is "Matthew Johnson"
    And an active employee
    And the employee's name is "Byron Sommardahl"
    When I retrieve employees where "last name" is "Johnson"
    Then all the employees should have "last name" with "Johnson"

  @Regression
  Scenario: Find Employees by Middle Name (uid:17362f0d-238e-453a-a7eb-e9e01dfd86c5)
    Given an active employee
    And the employee's name is "Jeff Allen Johnson William"
    And an active employee
    And the employee's name is "Matthew Johnson"
    And an active employee
    And the employee's name is "Byron Allen Sommardahl"
    When I retrieve employees where "middle name" is "Allen"
    Then all the employees should have "middle name" with "Allen"
