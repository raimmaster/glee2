Feature: modify employee
    In order to ___ as a ______ I can ______.

  Scenario: Cannot remove the last name (uid:33569901-38b9-484c-9870-fb6ca50d00ee)
    Given an active employee
    And the employee's name is "Jeff Alln Johnson William"
    When I attempt to modify his name to "Jeff"
    Then it should not be successful
    And the employee's name should be "Jeff Alln Johnson William"

  Scenario: Can modify 4 names to 2 (uid:fa4dbd51-8946-4bc0-a762-1ba2b5fa7dbf)
    Given an active employee
    And the employee's name is "Jeff Alln Johnson William"
    When I attempt to modify his name to "Jeff William"
    Then the employee's name should be "Jeff William"

  Scenario: Modify Display Name (uid:0b598d26-1845-4426-b0c1-9126c905d33e)
    Given an active employee
    And the employee's "display name" is "Bozo"
    When I modify his "display name" to "Zonkers"
    Then the employee's "display name" should be "Zonkers"

  Scenario: Modify Phone Number (uid:becec7f8-44b2-4414-98f0-6a731642e4a4)
    Given an active employee
    And the employee's "phone number" is "123"
    When I modify his "phone number" to "456"
    Then the employee's "phone number" should be "456"

  Scenario: Modify Company Email (uid:c41fc3f6-93ff-4975-a900-a86691222586)
    Given an active employee
    When I modify his "company email" to "d@j.com"
    Then the employee's "company email" should be "d@j.com"

  Scenario: Modify Personal Email (uid:8c4ea73d-dc3c-44d9-8955-e958d28468db)
    Given an active employee
    And the employee's "personal email" is "a@b.com"
    When I modify his "personal email" to "d@j.com"
    Then the employee's "personal email" should be "d@j.com"

  Scenario: Modify Address (uid:f50d65d0-17dc-4986-89cb-fe64869c4fe6)
    Given an active employee
    When I modify his address to "3001 Barrio Los Encuentros,Santa Ana,FM,Honduras"
    Then the employee's "address" should be "3001 Barrio Los Encuentros"
    And the employee's "city" should be "Santa Ana"
    And the employee's "region" should be "FM"
    And the employee's "country" should be "Honduras"

  Scenario: Modify Names (uid:b9d8485c-1638-47e4-bc85-1c4031d3bd93)
    Given an active employee
    When I modify his name to "Jeffry Allan Johnsen Williams"
    Then the employee's "first" name should be "Jeffry"
    And the employee's "middle" name should be "Allan"
    And the employee's "last" name should be "Johnsen"
    And the employee's "second last" name should be "Williams"
