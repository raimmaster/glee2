Feature: create employee
    In order to access my employee later as a member of peopleops I can create an employee.

  Scenario: Create a New Employee (uid:98ad2a8b-af2b-4d25-a7d4-0e9cafc1ba55)
    Given there are no employees with name "John Philip Williams"
    When I create an employee with name "John Philip Williams"
    Then the employee's "first" name should be "John"
    And the employee's "middle" name should be "Philip"
    And the employee's "last" name should be "Williams"

  Scenario: Can create employee with similar name (uid:39b036c2-50e6-4943-b8da-f3b16fe75237)
    Given an active employee
    And the employee's name is "Ralph Waldo Emmerson"
    When I create an employee with name "Ralph Waldo Emmerson Maya"
    Then the employee should exist

  Scenario: Attempt to Create Duplicate Employee (uid:63c72182-cbae-4b50-8949-7cb3804a7f47)
    Given an active employee
    And the employee's name is "Ralph Waldo Emmerson"
    When I attempt to create an employee with name "Ralph Waldo Emmerson"
    Then it should not be successful

  Scenario: Should not be able to create an employee without last name (uid:f6a1e615-97f5-424f-8750-612e7ac3714b)
    Given I want to create a new employee
    When I attempt to create an employee with name "Ralph"
    Then it should not be successful
