const {Given, Then, When} = require("cucumber");
const axios = require("axios");
const {expect} = require("chai");

Given("an active employee", function () {
    return 'pending';
});

Given("the employee's name is {string}", function (arg1) {
    return 'pending';
});

When("I modify his name to {string}", function (arg1) {
    return 'pending';
});

Then("the employee's first name should be {string}", function (arg1) {
    return 'pending';
});

Then("the employee's middle name should be {string}", function (arg1) {
    return 'pending';
});

Then("the employee's last name should be {string}", function (arg1) {
    return 'pending';
});

Then("the employee's second last name should be {string}", function (arg1) {
    return 'pending';
});
