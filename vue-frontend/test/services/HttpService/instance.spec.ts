import { Auth } from '@nuxtjs/auth'
import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { initializeHttpService } from '@/services/HttpService/instance'
import { HttpService } from '@/services/HttpService'

const axiosMethods = {
  get: jest.fn(),
  post: jest.fn(),
  delete: jest.fn(),
  put: jest.fn()
}
const mockLoggedInValue = false
const authMethods = {
  login: jest.fn(),
  getToken: jest.fn(),
  loggedIn: mockLoggedInValue
}
const useInterceptor = jest.fn()
const baseUrl = 'https://my-test-api.com'

describe('when calling initializeHttpService', () => {
  let nuxtAxiosInstance: NuxtAxiosInstance
  let authModule: Auth
  let service: HttpService
  const originalProcessEnv = { ...process.env }

  beforeAll(() => {
    process.env.BACKEND_URL = baseUrl
  })
  afterAll(() => {
    process.env = { ...originalProcessEnv }
  })
  beforeEach(() => {
    jest.clearAllMocks()
    nuxtAxiosInstance = jest.fn<NuxtAxiosInstance, []>().mockReturnValue({
      // @ts-ignore
      create: jest.fn(() => ({
        interceptors: { response: { use: useInterceptor } },
        ...axiosMethods
      }))
    })()
    // @ts-ignore
    authModule = jest.fn<Auth, []>(() => ({ ...authMethods }))()
  })
  it('should set httpService instance', () => {
    // @ts-ignore
    service = new HttpService(nuxtAxiosInstance, authModule)
    const httpService = initializeHttpService(service)
    expect(httpService).toEqual(service)
  })
})
