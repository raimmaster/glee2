import { withKnobs } from '@storybook/addon-knobs'
import ActionButtons from './ActionButtons.vue'

export default {
  title: 'ActionButtons',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { ActionButtons },
  data() {
    return {
      employee: '0',
      activeTab: 'Active'
    }
  },
  methods: {
    onEdit() {},
    onDeactivate() {},
    onActivate() {}
  },
  template: `
    <action-buttons :employee="'0'" :activeTab="activeTab"  @editEmployee="onEdit"
    @deactivateEmployee="onDeactivate"
    @activateEmployee="onActivate" />
  `
})
