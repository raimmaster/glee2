import { withKnobs } from '@storybook/addon-knobs'
import TableHeader from './TableHeader.vue'

export default {
  title: 'TableHeader',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { TableHeader },
  methods: {
    setActiveTab() {}
  },
  template: `
    <TableHeader  @active="setActiveTab" />
  `
})
