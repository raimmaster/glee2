import { withKnobs } from '@storybook/addon-knobs'
import Table from './Table.vue'

export default {
  title: 'Table',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { Table },
  data() {
    return {
      tableFields: [
        'avatar',
        'displayName',
        'lastName',
        'companyEmail',
        'tags',
        'birthdate',
        'startDate',
        'actions'
      ],
      items: [
        {
          avatar: 'retro',
          displayName: 'John',
          lastName: 'Doe',
          companyEmail: 'john@doe.com',
          birthdate: '1998-05-21',
          startDate: '2019-01-01',
          isActive: 'true'
        },
        {
          avatar: 'retro',
          displayName: 'Jane',
          lastName: 'Doe',
          companyEmail: 'jane@doe.com',
          birthdate: '1997-02-21',
          startDate: '2020-01-01',
          isActive: 'false'
        }
      ],
      activeTab: 'Active'
    }
  },
  methods: {
    getTags() {},
    onEdit() {},
    onDeactivate() {},
    onActivate() {}
  },
  template: `
    <Table :items="items" :table-fields="tableFields" :active-tab="activeTab" @getTags="getTags"
    @editEmployee="onEdit"
    @deactivateEmployee="onDeactivate"
    @activateEmployee="onActivate" />
  `
})
