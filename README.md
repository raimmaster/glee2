# Welcome

Glee2 is a monorepository that includes 2 different backends and 2 different frontends. After you fork the repository, you should delete the files of the backend and frontend you won't use, in addition to their pipeline jobs.

## CI/CD

This repository is setup with a full CI/CD pipeline through Gitlab and AWS.

### Review Apps

When you create a branch, Gitlab will kick off a build and eventually spin up a dedicated and temporary environment for that branch (frontend in S3, backend in EC2-ELB). It will also report back to the corresponding merge request with URL's for frontend and backend (shown in the end logs of deploy jobs). Your QA team and others will be able to test your feature branch in isolation and fully verify, validate and approve before merging back in to the origin branch.

<ins>Notes:</ins>

- Each branch will have its own database in a shared RDS instance and will have no data in the beginning.
- Changes in frontend or backend are detected and only deployed if necessary.

### Auth0 Setup

If you don't know how to work with Auth0's dashboard frontend, please follow [this fantastic guide](https://docs.google.com/document/d/1uOnm5o71JTQK5JuILydAeYQKLqvP6608rb7_cn2p_xk/edit) made by one of our developers on how to set up Auth0.

### Setup

Before you start you need a few things first:

- Auth0 tenant, one for each environment (We recommend you not use the same one for production and review apps)
- AWS ECR to store the docker images
- AWS RDS instance for the databases
- Sonarqube server and an account in Cucumber Studio

Once you have all the things mentioned, follow these **steps:**

1. Fork this project into your account
2. Remove fork relationship
3. Choose your backend/frontend techs, let's say you choose _React-Node_, then you will have to:
   1. **delete** `dotnet-backend/` and `vue-frontend/` folders
   2. **delete** `ci/gitlab/backend/backend-ec2-dotnet/` folder
   3. **delete** `ci/gitlab/frontend/vuejs-frontend/` folder
   4. **delete** all local imports for Vue and Dotnet in `.gitlab-cy.yml`
   5. **delete** the line `- if [ "${CI_COMMIT_REF_NAME}" = "staging" ]; then TYPEORM_HOST=<xxxxx>; fi` from `05-deploy/deploy-terraform-backend.yml`
4. Add the following project variables to your Gitlab:

   **for terraform files**

   - `AWS_ACCESS_KEY_ID` - This is the AWS access key (e.g. XXXXXXXXXXXXXX)
   - `AWS_SECRET_ACCESS_KEY` - This is the AWS secret access key (e.g XXXXXXXXXXXXXX)
   - `AWS_REGION` - This is the AWS region (e.g. us-east-1)
   - `VPC_CIDR` - 10.0.0.0/16
   - `SSH_KEY_NAME` - key pair name for the instances (e.g. My-Key-Name)
   - `DEV_INSTANCE_TYPE` - type of the instance for the bastion (e.g. t2.micro)
   - `DEV_AMI` - ami id (e.g. ami-0a389cf6bb534ebd8)
   - `ELB_HEALTHY_THRESHOLD` - Load Balancer healthy threshold number (e.g. 2)
   - `ELB_UNHEALTHY_THRESHOLD` - Loab Balancer unhealthy threshold number (e.g. 2)
   - `ELB_TIMEOUT` - Load Balancer timeout (e.g. 3)
   - `ELB_INTERVAL` - Load Balancer interval seconds (e.g. 30)
   - `LC_INSTANCE_TYPE` - type of instances of the autoscaling group (e.g. t2.small)
   - `ZONE_ID` - Route 53 zone id (e.g. XXXXXXX)
   - `HOSTED_ZONE_NAME` - Route 53 hosted zone name (e.g. mydomain.com)
   - `S3_BUCKET_TF_STATE` - bucket name to save terraform state files (e.g. tf-states-bucket)
   - `MIN_SIZE_ALB` - Load Balancer minimum number of instances (e.g. 1)
   - `MAX_SIZE_ALB` - Load Balancer maximum number of instances (e.g. 2)
   - `ALLOWED_SSH_IP` - Ipv4 addresses allowed to connect to bastion (e.g. 0.0.0.0/0)
   - `RDS_DB_PORT` - rds port (e.g. 5432)
   - `AWS_CERTIFICATE_ARN` - domain SSL certificate (e.g. arn:aws:acm:us-east-1:xxxxxxxx:certificate/xxxx-xxxx-xxxx-xxxx-xxxxx)
   - `TAGS` - Two tags are added to each resource.
     - `Name`: Name Prefix which is a the repo name + branch name
     - `Project`: Is the repo name

   **NodeJS variables**

   - `NODE_ENV` - production
   - `TYPEORM_CONNECTION` - Allowed values: https://github.com/typeorm/typeorm/blob/master/docs/connection-options.md#common-connection-options (e.g. postgres)
   - `RDS_DB_HOST` - your db host url (e.g. glee2-xxxx.xxxx.us-east-1.rds.amazonaws.com)
   - `TYPEORM_USERNAME` - your db username (e.g. my-db-user)
   - `TYPEORM_PASSWORD` - your db password (e.g. my-db-password)
   - `TYPEORM_PORT` - your db port (e.g. 5432)
   - `TYPEORM_ENTITIES` - ./src/modules/\*_/_.entity.ts
   - `TYPEORM_MIGRATIONS` - ./src/migrations/\*.ts
   - `AUTH0_CLIENT_SECRET` - secret key of auth0 (e.g. xxxxxxxxxxxxxxx)
   - `AUTH0_CLIENT_ID_DEV` - auth0 client id (e.g. xxxxxxxxxxxxxx)
   - `AUTH0_DOMAIN_DEV` - domain of your auth0 (e.g. mydomain.auth0.com)
   - `AUTH0_AUDIENCE_DEV` - audience of auth0 (e.g. https://myaudience.com)

   **Dotnet variables**

   - `JWT_ISSUER` - Auth0 domain value (e.g. mydomain.auth0.com)
   - `JWT_AUDIENCE` - Auth0 audience value (e.g. https://myaudience.com)
   - `RDS_DB_HOST` - your db host url (e.g. glee2-xxxx.xxxx.us-east-1.rds.amazonaws.com)
   - `RDS_DB_PORT` - your db port (e.g. 5432)
   - `RDS_DB_USER` - your db username (e.g. my-db-user)
   - `RDS_DB_PASSWORD` - your db password (e.g my-db-password)

   **Frontend Variables (any)**

   - `HOSTED_ZONE_NAME` - same as terraform variable (e.g. mydomain.com)
   - `AUTH0_CLIENT_ID_DEV` - your app client ID (e.g. xxxxxxxxxxxxx)
   - `AUTH0_DOMAIN` - your app client domain (e.g. mydomain.auth0.com)
   - `AUTH0_AUDIENCE` - your app client audience (e.g. https://myaudience.com)

   **Cucumber variables**

   - `AUTH0_DOMAIN_DEV` - domain of your auth0 app (e.g. mydomain.auth0.com)
   - `AUTH0_CLIENT_ID_DEV` - auth0 client id (e.g. xxxxxxxxxxxxx)
   - `AUTH0_AUDIENCE_DEV` - audience of auth0 (e.g. https://myaudience.com)
   - `AUTH0_CLIENT_USER` - auth0 username of a user (e.g. my-user)
   - `AUTH0_CLIENT_PASS` - auth0 password of the same user (e.g. my-password)
   - `HOSTED_ZONE_NAME` - same as terraform variable (e.g. mydomain.com)

   **Sonarqube variables**

   - `SONAR_LOGIN` - sonarqube login token (e.g. xxxxxxxxxxxx)
   - `SONAR_URL` - sonarqube server url (e.g. http://sonarqube.mydomain.com:9000)
   - `GITLAB_USER_TOKEN` - gitlab access token (e.g. xxxxxxxxxxxxxx)

   **Other variables needed for deployment jobs**

   - `ANSIBLE_CONFIG` = ansible.cfg
   - `AGENT_SSH_KEY` = content of the key pair used for terraform instances (e.g. -----BEGIN RSA PRIVATE KEY-----xxxxxx)
   - `AWS_ECR_REGISTRY` = ECR uri used to store the review apps docker images (e.g. xxxxx.dkr.ecr.us-east-1.amazonaws.com/xxxxx)

5. Run your pipeline!

## Technical Debts

- [x] S3 tfstates are not deleted automatically.
- [x] Speed up RDS instance creation
- [ ] Speed up AMI creation for Autoscaling Group
- [ ] Validations for a production environment
