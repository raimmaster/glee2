provider "aws" {
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key

}

terraform {
  backend "s3" {
    #bucket is set in cli
    region = "us-east-1"
  }
}

module "network-sprint-0" {
  source      = "AcklenAvenue/modules/aws//modules/network"
  vpc_cidr    = var.vpc_cidr
  name_prefix = var.name_prefix

  cidrs = {
    public1  = "10.0.1.0/24"
    public2  = "10.0.2.0/24"
    private1 = "10.0.3.0/24"
    private2 = "10.0.4.0/24"
    rds1     = "10.0.5.0/24"
    rds2     = "10.0.6.0/24"
  }

}

module "security-group-sprint0" {
  source = "AcklenAvenue/modules/aws//modules/security"
  # insert the 5 required variables here
  name_prefix          = var.name_prefix
  vpc_id               = module.network-sprint-0.vpc_id
  private_ingress_cidr = [var.vpc_cidr]
  allowed_ssh_ip       = [var.allowed_ssh_ip]
  db_port              = var.db_port
}

module "cloudwatch" {
  source      = "AcklenAvenue/modules/aws//modules/cloudwatch"
  name_prefix = var.name_prefix
}

module "ec2-sprint0" {
  source            = "./modules/ec2"
  dev_instance_type = var.dev_instance_type
  dev_ami           = var.dev_ami
  name_prefix       = var.name_prefix
  key_name          = var.key_name

  public1_subnet_id  = module.network-sprint-0.public1_subnet_id
  public2_subnet_id  = module.network-sprint-0.public2_subnet_id
  private1_subnet_id = module.network-sprint-0.private1_subnet_id
  private2_subnet_id = module.network-sprint-0.private2_subnet_id
  bastion_sg_id      = module.security-group-sprint0.bastion_sg_id
  sprint_private_sg  = module.security-group-sprint0.sprint0_private_sg

  lc_instance_type = var.lc_instance_type
  project          = var.project
}

module "ami" {
  source      = "./modules/ami"
  name_prefix = var.name_prefix
  image_id    = module.ec2-sprint0.ec2_id
  project     = var.project
}

module "application-lb" {
  source                  = "./modules/loadbalancer/application"
  name_prefix             = var.name_prefix
  public1_subnet_id       = module.network-sprint-0.public1_subnet_id
  public2_subnet_id       = module.network-sprint-0.public2_subnet_id
  sprint0_public_sg       = module.security-group-sprint0.sprint0_public_sg
  vpc_id                  = module.network-sprint-0.vpc_id
  elb_healthy_threshold   = var.elb_healthy_threshold
  elb_unhealthy_threshold = var.elb_unhealthy_threshold
  elb_timeout             = var.elb_timeout
  elb_interval            = var.elb_interval
  zone_id                 = var.zone_id
  project                 = var.project
}

module "autoscaling" {
  source                    = "./modules/autoscaling"
  name_prefix               = var.name_prefix
  image_id                  = module.ami.ami_id
  lc_instance_type          = var.lc_instance_type
  key_name                  = var.key_name
  sprint_private_sg         = module.security-group-sprint0.sprint0_private_sg
  private1_subnet_id        = module.network-sprint-0.private1_subnet_id
  private2_subnet_id        = module.network-sprint-0.private2_subnet_id
  min_size_alb              = var.min_size_alb
  max_size_alb              = var.max_size_alb
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  app-lb-tgt-atscaling      = module.application-lb.app_lb_tgt_atscaling
  project                   = var.project

}
