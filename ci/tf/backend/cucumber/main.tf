provider "aws" {
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key

}

data "aws_acm_certificate" "certificate" {
  domain   = "*.acklenavenueclient.com"
  statuses = ["ISSUED"]
}

data "aws_lb" "application-loadbalancer" {
  name = "${var.name_prefix}-app-lb"
}

data "aws_vpc" "sprint0_vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.name_prefix}-vpc"]
  }
}

data "aws_instances" "asg_ec2" {
  instance_tags = {
    Name = "${var.name_prefix}-ec2-autoscaling"
  }
}

data "aws_security_group" "sprint0_public_sg" {
  name = "${var.name_prefix}-public_sg"
}

resource "aws_lb_target_group" "app_cucumber" {
  name     = "${var.name_prefix}-cucumbertest"
  port     = 4000
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.sprint0_vpc.id

  health_check {
    protocol            = "HTTP"
    path                = "/api/status"
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
  }

  tags = {
    Name = "${var.name_prefix}-app-tgt-gp"
  }
}

resource "aws_lb_target_group_attachment" "register_cucumber" {
  target_group_arn = aws_lb_target_group.app_cucumber.arn
  # register one instance of asg to the target group
  target_id = data.aws_instances.asg_ec2.ids[0]
  port      = 4000
}

resource "aws_lb_listener" "app-lb-listener-4000" {
  load_balancer_arn = data.aws_lb.application-loadbalancer.arn
  port              = "4000"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = data.aws_acm_certificate.certificate.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_cucumber.arn
  }
}

resource "aws_security_group_rule" "port_cucumber" {
  type              = "ingress"
  from_port         = 4000
  to_port           = 4000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = data.aws_security_group.sprint0_public_sg.id
}