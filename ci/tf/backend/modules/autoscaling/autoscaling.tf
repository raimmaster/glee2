resource "aws_launch_configuration" "launchconfiguration" {
  name_prefix     = "${var.name_prefix}-launchconfig"
  image_id        = var.image_id
  instance_type   = var.lc_instance_type
  key_name        = var.key_name
  security_groups = [var.sprint_private_sg]
}

resource "aws_autoscaling_group" "autoscaling-project" {
  name                      = "${var.name_prefix}-auto"
  vpc_zone_identifier       = [var.private1_subnet_id, var.private2_subnet_id]
  launch_configuration      = aws_launch_configuration.launchconfiguration.name
  min_size                  = var.min_size_alb
  max_size                  = var.max_size_alb
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  #load_balancers            = [var.classic_lb_name]
  target_group_arns = [var.app-lb-tgt-atscaling]
  force_delete      = true

  tag {
    key                 = "Name"
    value               = "${var.name_prefix}-ec2-autoscaling"
    propagate_at_launch = true
  }

  tag {
    key                 = "Project"
    value               = var.project
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_schedule" "schedule-turnoff" {
  autoscaling_group_name = aws_autoscaling_group.autoscaling-project.name
  scheduled_action_name  = "Shutdown-Mon-Friday"
  recurrence             = "0 0 * * 2-6" #2020-03-09 00:00:00 UTC - 18:00:00
  min_size               = 0
  max_size               = var.max_size_alb
  desired_capacity       = 0
}

resource "aws_autoscaling_schedule" "schedule-turnon" {
  autoscaling_group_name = aws_autoscaling_group.autoscaling-project.name
  scheduled_action_name  = "Turn on-Mon-Friday"
  recurrence             = "30 12 * * 2-6" #e.g 2020-03-09 12:30:00 UTC - 06:30:00
  min_size               = var.min_size_alb
  max_size               = var.max_size_alb
  desired_capacity       = var.min_size_alb
}
