#------------ load balancer ------------

data "aws_acm_certificate" "certificate" {
  domain   = "*.acklenavenueclient.com"
  statuses = ["ISSUED"]
}

resource "aws_elb" "sprint0_elb" {
  name = "${var.name_prefix}-elb1"

  subnets = [var.public1_subnet_id,
    var.public2_subnet_id,
  ]

  security_groups = [var.sprint0_public_sg]

  listener {
    instance_port     = 5000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 5000
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = data.aws_acm_certificate.certificate.arn
  }

  health_check {
    healthy_threshold   = var.elb_healthy_threshold
    unhealthy_threshold = var.elb_unhealthy_threshold
    timeout             = var.elb_timeout
    target              = "TCP:5000"
    interval            = var.elb_interval
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "${var.name_prefix}-elb"
  }
}

resource "aws_route53_record" "backend-dns-record" {
  zone_id = var.zone_id
  name    = "${var.name_prefix}.acklenavenueclient.com"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_elb.sprint0_elb.dns_name]

}
