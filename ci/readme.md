## CI Setup

### AWS Setup

#### Route53

The domain name should be under the control of Route53 since new CNAME entries are created using Terraform.

#### Certificate Manager

Create a "wildcard certificate" using `*.domainname.com` instead of just `domainname.com`. The ARN of that cert should be added to the CI/CD Environment Variables using key `AWS_CERTIFICATE_ARN`.

#### RDS

For db automation to work in CI/CD, you will need a Postgres RDS instance already created (one for staging and one for review apps preferable). The connection details should be filled in `RDS_DB` or `TYPEORM` prefix variables; depending on which backend you want to use. The review apps RDS configuration should allow public access.
On the other hand, you **should (manually) create the db and move your staging RDS** instance to the private subnet created for you with the terraform modules.
Also, delete the line `- if [ "${CI_COMMIT_REF_NAME}" = "staging" ]; then RDS_DB_HOST="glee2-<xxxxx>;" fi` from `ci/---/05-deploy/deploy-terraform-backend.yml`

#### Container Registry

Terraform won't create your ECR, you should create it beforehand and add its URI in the `AWS_ECR_REGISTRY` Gitlab CI/CD variables.

#### Key pair

Terraform won't create your EC2s key pair either. You should create it yourself as a pem format. Then, fill `SSH_KEY_NAME` with it's name and `AGENT_SSH_KEY` with the pem file content.