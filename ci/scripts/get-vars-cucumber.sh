#! /bin/bash

# Varibles For Cucumber
export AUTH0_CLIENT_ID=$AUTH0_FE_CLIENT_ID_DEV
export AUTH0_DOMAIN=$AUTH0_DOMAIN_DEV
export AUTH0_AUDIENCE=$AUTH0_AUDIENCE_DEV
export AUTH0_CLIENT_USER=$AUTH0_CLIENT_USER
export AUTH0_CLIENT_PASS=$AUTH0_CLIENT_PASS
export BACKEND_URL=http://localhost:3030

# Variables for Nodejs Backend
export TYPEORM_CONNECTION=postgres
export TYPEORM_ENTITIES=./src/modules/domain/**/*.entity.ts
export TYPEORM_MIGRATIONS=./src/migrations/*.ts
export TYPEORM_HOST=postgres
export TYPEORM_PORT=5432
export TYPEORM_USERNAME=postgres
export TYPEORM_PASSWORD=postgres
export TYPEORM_DATABASE=glee2

# Variables for Dotnet Backend
export DB_CONNECTION="Host=postgres;Database=glee2;Username=postgres;Password=postgres"
export JWT_ISSUER=https://${AUTH0_DOMAIN_DEV}/
export JWT_AUDIENCE=$JWT_AUDIENCE