from string import Template
import os
#open the file
filein = open( '../tf/backend/terraform' )
#read it
src = Template( filein.read() )
#document data
name_prefix = '"{}"'.format(os.environ['NAME_PREFIX'])
access_key = '"{}"'.format(os.environ['AWS_ACCESS_KEY_ID'])
secret_key = '"{}"'.format(os.environ['AWS_SECRET_ACCESS_KEY'])
aws_region = '"{}"'.format(os.environ['AWS_REGION'])
vpc_cidr = '"{}"'.format(os.environ['VPC_CIDR'])
key_name = '"{}"'.format(os.environ['SSH_KEY_NAME'])
dev_instance_type = '"{}"'.format(os.environ['DEV_INSTANCE_TYPE'])
dev_ami = '"{}"'.format(os.environ['DEV_AMI'])
elb_healthy_threshold = '"{}"'.format(os.environ['ELB_HEALTHY_THRESHOLD'])
elb_unhealthy_threshold = '"{}"'.format(os.environ['ELB_UNHEALTHY_THRESHOLD'])
elb_timeout = '"{}"'.format(os.environ['ELB_TIMEOUT'])
elb_interval = '"{}"'.format(os.environ['ELB_INTERVAL'])
lc_instance_type = '"{}"'.format(os.environ['LC_INSTANCE_TYPE'])
zone_id = '"{}"'.format(os.environ['ZONE_ID'])
hosted_zone_name = '"{}"'.format(os.environ['HOSTED_ZONE_NAME'])
application = '"{}"'.format(os.environ['CI_PROJECT_NAME'])
environment = '"{}"'.format(os.environ['CI_COMMIT_REF_NAME'])
project = '"{}"'.format(os.environ['CI_PROJECT_NAME'])
branch = '"{}"'.format(os.environ['CI_COMMIT_REF_NAME'])
min_size_alb = '"{}"'.format(os.environ['MIN_SIZE_ALB'])
max_size_alb = '"{}"'.format(os.environ['MAX_SIZE_ALB'])
allowed_ssh_ip = '"{}"'.format(os.environ['ALLOWED_SSH_IP'])
db_port = '"{}"'.format(os.environ['RDS_DB_PORT'])

d = { "name_prefix": name_prefix, "access_key": access_key, "secret_key": secret_key, "aws_region": aws_region,
    "vpc_cidr": vpc_cidr, "key_name": key_name, "dev_instance_type": dev_instance_type, "dev_ami": dev_ami,
    "elb_healthy_threshold": elb_healthy_threshold, "elb_unhealthy_threshold": elb_unhealthy_threshold,
    "elb_timeout": elb_timeout, "elb_interval": elb_interval, "lc_instance_type": lc_instance_type,
    "zone_id": zone_id, "environment": environment, "application": application, "project": project,
    "branch": branch, "min_size_alb": min_size_alb, "max_size_alb": max_size_alb,
    "hosted_zone_name": hosted_zone_name, "allowed_ssh_ip": allowed_ssh_ip,
    "db_port": db_port
 }


#do the substitution
result = src.substitute(d)

file = open("../tf/backend/terraform.tfvars", "w")
file.write(result)
file.close()
