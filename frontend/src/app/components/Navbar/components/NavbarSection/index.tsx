import * as React from 'react';
import style from '../../../../style.local.css';
import { NavbarItem } from '../NavbarItem';

export declare namespace NavbarSection {
  export interface Props {
    className?: string;
    dataAccordionItem?: string;
    href?: string;
    onClick?: any;
    children?: any;
    navbarItems: Array<any>;
  }
}

const renderNavbarItems = (navbarItems: Array<any>) => {
    return navbarItems.map((navbarItem, index) => 
        <NavbarItem 
            className={navbarItem.className}
            href={navbarItem.href}
            onClick={navbarItem.onClick}
            key={index}
        >
            {navbarItem.children()}
        </NavbarItem>
    )
}

export const NavbarSection = (props: NavbarSection.Props) => {
  return (
    <li className={props.className || ''} data-accordion-item={props.dataAccordionItem || ''}>
        <a href={props.href || '#'} onClick={props.onClick || null} className={style['accordion-title']}>
            {props.children}
        </a>
        <ul className={`${style.menu} ${style.vertical} ${style.nested}`}>
            {renderNavbarItems(props.navbarItems)}
        </ul>
    </li>
  );
};