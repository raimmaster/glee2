export interface SortedColumn {
  id: string;
  desc: boolean;
}
